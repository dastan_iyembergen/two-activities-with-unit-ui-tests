package com.example.twoactivitiesuitests;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    EditText firstNameEdit;
    EditText lastNameEdit;
    TextView messageView;
    public AppCompatActivity activity;
    public static String FIRST_NAME = "FIRST_NAME";
    public static String LAST_NAME = "LAST_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        firstNameEdit = findViewById(R.id.firstNameEdit);
        lastNameEdit = findViewById(R.id.lastNameEdit);
        messageView = findViewById(R.id.messageView);
        activity = this;
        Button submitButton = findViewById(R.id.submitButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, SecondActivity.class);
                String firstName = firstNameEdit.getText().toString();
                String lastName = lastNameEdit.getText().toString();
                if (!check_Name(firstName) || !check_Name(lastName)){
                    messageView.setText("incorrect first or last name");
                    return;
                }
                messageView.setText("");
                intent.putExtra(FIRST_NAME, firstName);
                intent.putExtra(LAST_NAME, lastName);
                startActivity(intent);
            }
        });
    }

    static public boolean check_Name(String s){
        return Pattern.matches("[A-Z]([a-z])*", s);
    }
}
