package com.example.twoactivitiesuitests;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void test_name_validation(){
        String test_name = "Dastan";
        assertEquals(true, MainActivity.check_Name(test_name));

        test_name = "D";
        assertEquals(true, MainActivity.check_Name(test_name));

        test_name = "dastan";
        assertEquals(false, MainActivity.check_Name(test_name));

        test_name = "DAstan";
        assertEquals(false, MainActivity.check_Name(test_name));

        test_name = "d";
        assertEquals(false, MainActivity.check_Name(test_name));

        test_name = "DASTAN";
        assertEquals(false, MainActivity.check_Name(test_name));

        test_name = "Dastan56";
        assertEquals(false, MainActivity.check_Name(test_name));

        test_name = "Da5tan";
        assertEquals(false, MainActivity.check_Name(test_name));
    }
}