package com.example.twoactivitiesuitests;

import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class IntentTest {
    @Rule
    public IntentsTestRule<MainActivity> mIntentsRule =
            new IntentsTestRule<>(MainActivity.class);

    @Test
    public void test_2 () {
        onView(withId(R.id.firstNameEdit))
                .perform(typeText("Dastan"));
        onView(withId(R.id.lastNameEdit))
                .perform(typeText("Iyembergen"));
        onView(withId(R.id.submitButton))
                .perform(click());
        intended(hasComponent(hasShortClassName(".SecondActivity")));
        onView(withId(R.id.dataText))
                .check(matches(withText("Hello, Dastan Iyembergen")));
    }
}
