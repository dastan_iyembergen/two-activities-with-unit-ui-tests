package com.example.twoactivitiesuitests;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class FirstUITest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void test_1 () {
        onView(withId(R.id.firstNameEdit))
                .perform(typeText("dastan"));
        onView(withId(R.id.lastNameEdit))
                .perform(typeText("Iyembergen"));
        onView(withId(R.id.submitButton))
                .perform(click());
        onView(withId(R.id.messageView))
                .check(matches(withText("incorrect first or last name")));
    }

}
