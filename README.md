# Application with two Activities and Unit/UI tests
## App Usage:
Enter First & Last names and click SUBMIT.  
Names should be latin characters with one uppercase letter at the beginning followed by lowercase letters.  
Otherwise, the error message will be shown.  
If the names are correct, the second activity will be shown with text "Hello, " + your names.  
## Unit Test
Unit test is: app/src/test/java/ExampleUnitTest.  
Tests name validation method.  
## UI Tests:
Implemented using espresso  
UI Tests:  
- app/src/androidTest/java/FirstUITest - tests the error message  
- app/src/androidTest/java/IntentTest - tests the intent to the second activity and the message in the second activity.
 